﻿using SignalRDemo.Client;

Console.WriteLine("Enter any key to start messaging...");
Console.ReadKey();

const string baseUri = "https://localhost:7162/";
var client1 = new Client("client_1", baseUri);
var client2 = new Client("client_2", baseUri);

await client1.ConnectToHubAsync();
await client2.ConnectToHubAsync();

var canExit = false;

client1.OnReceive(async (string user, string message) =>
{
	Console.WriteLine($"client_1 has received message from {user}: {message}");
	await client1.SendMessageAsync("I've got your message");
});

client2.OnReceive(async (string user, string message) =>
{
	Console.WriteLine($"client_2 has received message from {user}: {message}");
	if (!message.Equals("I've got your message", StringComparison.OrdinalIgnoreCase))
		await client2.SendMessageAsync("bye");
	else
		canExit = true;
});

await client1.SendMessageAsync("Hello client 2");

while (!canExit) {}
await client1.DisconectAsync();
await client2.DisconectAsync();
