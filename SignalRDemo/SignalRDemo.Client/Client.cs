﻿using Microsoft.AspNetCore.SignalR.Client;

namespace SignalRDemo.Client;

public sealed class Client
{
	private readonly string userName;

	private readonly HttpClient httpClient;
	private HubConnection? hubConnection;

	public Client(string userName, string baseUri)
	{
		this.userName = userName;
		httpClient = new HttpClient
		{
			BaseAddress = new Uri(baseUri)
		};
	}

	public async Task ConnectToHubAsync()
	{
		var response = await httpClient.GetAsync("/get-connection");
		if (response is null || !response.IsSuccessStatusCode)
		{
			Console.WriteLine("Response is not success");
			return;
		}

		var baseAddress = await response.Content.ReadAsStringAsync();
		baseAddress = baseAddress.Trim(new[] { '"', ' ' });

		hubConnection = new HubConnectionBuilder()
			.WithUrl(baseAddress)
			.Build();

		await hubConnection.StartAsync();
	}

	public void OnReceive(Action<string, string> outputMessage)
	{
		if (hubConnection is null)
			throw new ArgumentNullException(nameof(hubConnection));

		hubConnection.On("ReceiveMessage", outputMessage);
	}

	public void OnAsyncReceive(Func<string, string, Task> outputMessageAsync)
	{
		if (hubConnection is null)
			throw new ArgumentNullException(nameof(hubConnection));

		hubConnection.On("ReceiveMessage", outputMessageAsync);
	}

	public async Task SendMessageAsync(string message)
	{
		if (hubConnection is null)
			throw new ArgumentNullException(nameof(hubConnection));

		await hubConnection.SendAsync("SendMessage", userName, message);
	}

	public ValueTask DisconectAsync()
	{
		return hubConnection?.DisposeAsync() ?? ValueTask.CompletedTask;
	}
}
