﻿using Microsoft.AspNetCore.SignalR;

namespace SignalRDemo.Server;

public sealed class ChatHub : Hub
{
	private readonly ILogger<ChatHub> logger;

	public ChatHub(ILogger<ChatHub> logger)
	{
		this.logger = logger;
	}

	public async Task SendMessage(string user, string message)
	{
		logger.LogInformation($"{user} sent a message to all: {message}");
		await Clients.AllExcept(Context.ConnectionId).SendAsync("ReceiveMessage", user, message);
	}
}
