using SignalRDemo.Server;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSignalR();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.MapGet("/get-connection", (HttpContext httpContext) => 
{
	return Results.Ok("https://localhost:7162/chat");
});

app.MapHub<ChatHub>("/chat");

app.UseHttpsRedirection();
app.UseAuthorization();

app.Run();
