﻿using AsyncComplexSocketDemo.Clients;

await Task.Delay(1000);
//var client = new Client(1);
//client.Connect();
//client.SendAndReceive("Hello from client " + client.Id);
//client.Send("exit");
//client.Disconnect();

var clients = Enumerable.Range(1, 10).Select(id => new Client(id));
Parallel.ForEach(clients, client =>
{
    var random = new Random();
    client.Connect();

    var sleepTime = random.Next(1000, 5000);
    Console.WriteLine($"Client {client.Id} will sleep for {sleepTime} ms");
    Thread.Sleep(sleepTime);

    var messageCount = random.Next(1, 10);
    for (var i = 0; i < messageCount; i++)
        client.SendAndReceive("Hello from client " + client.Id);
    client.Send("exit");

    client.Disconnect();
});

Console.ReadKey();
