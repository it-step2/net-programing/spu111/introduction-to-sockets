﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace AsyncComplexSocketDemo.Clients;

public class Client
{
    private const string DefaultIpAddress = "127.0.0.1";
    private const int DefaultPort = 5000;
    
    private readonly EndPoint endPoint;
    private Socket? client;

    public int Id { get; }

    public Client(int id, string ipAddressString = DefaultIpAddress, int port = DefaultPort)
    {
        Id = id;

        var ipAddress = IPAddress.Parse(ipAddressString);
        Console.WriteLine(ipAddress.ToString());
        endPoint = new IPEndPoint(ipAddress, port);
    }

    public void Connect()
    {
        var isSuccessfull = false;
        while (!isSuccessfull)
        {
            try
            {
                client = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                client.Connect(endPoint);
                isSuccessfull = true;
            }
            catch (SocketException)
            {
                Thread.Sleep(1000);
            }
        }
    }

    public void Send(string message)
    {
        if (client is null || !client.Connected)
            Connect();

        var bytes = Encoding.UTF8.GetBytes(message);
        client!.Send(bytes);
        Console.WriteLine("Sent: " + message);
    }

    public string Receive()
    {
        if (client is null || !client.Connected)
            Connect();

        var buffer = new byte[1024];
        var receivedBytes = client!.Receive(buffer);
        var receivedMessage = Encoding.UTF8.GetString(buffer, 0, receivedBytes);
        Console.WriteLine("Received: " + receivedMessage);
        return receivedMessage;
    }

    public string SendAndReceive(string message)
    {
        Send(message);
        return Receive();
    }

    public void Disconnect()
    {
        client!.Shutdown(SocketShutdown.Both);
        client.Close();
        client = null;
    }

    public void Dispose()
    {
        Disconnect();
        client?.Dispose();
    }

}