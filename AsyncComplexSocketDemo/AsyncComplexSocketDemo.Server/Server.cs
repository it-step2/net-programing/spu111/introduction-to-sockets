using System.Net;
using System.Net.Sockets;
using System.Text;

namespace AsyncComplexSocketDemo.Server;

public class Server
{
	private readonly IPEndPoint ipEndPoint;
	private Socket? socket;
	private bool isStarted;

	private readonly Encoding encoding;

	private Task? startTask;

	public Server(string ip, int port, Encoding? encoding = null)
	{
		ipEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
		isStarted = false;
		this.encoding = encoding ?? Encoding.UTF8;
	}

	public void Start()
	{
		if (socket is not null)
			throw new InvalidOperationException("Server is already started");

		socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		socket.Bind(ipEndPoint);
		socket.Listen(10);

		startTask = StartServerAsync();
	}

	private async Task StartServerAsync()
	{
		Console.WriteLine("Server is waiting for connections");
		isStarted = true;
		while (isStarted)
		{
			try
			{
				var client = await socket!.AcceptAsync();
				Console.WriteLine("Start communication with client " + client.RemoteEndPoint);
				await CommunicateAsync(client);
			}
			catch (SocketException ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}

	public async void Stop()
	{
		if (socket is null || startTask is null)
			return;

		socket.Close();
		socket = null;
		isStarted = false;
		await startTask;
	}


	private async Task CommunicateAsync(Socket client)
	{
		var message = string.Empty;
		while (ContinueCommunication(message))
		{
			var buffer = new byte[1024];
			var receivedBytesNumber = await client.ReceiveAsync(buffer);
			message = encoding.GetString(buffer, 0, receivedBytesNumber);
			Console.WriteLine(client.RemoteEndPoint + ": " + message);

			var response = "Hello from server"u8.ToArray();
			await client.SendAsync(response);
		}
	}

	private static bool ContinueCommunication(string message) =>
		!message.Equals("exit", StringComparison.OrdinalIgnoreCase);
}