﻿using AsyncComplexSocketDemo.Server;

var server = new Server("127.0.0.1", 5000);
server.Start();

Console.WriteLine("Press any key to stop server");
Console.ReadKey();
server.Stop();
