﻿#include <iostream>
#include <string>
#include "WinSock2.h" // тут знаходяться оголошення, необхідні
// для Winsock 2 API.
#include <ws2tcpip.h> // містить функції для роботи з адресами
                      // напр. inet_pton
#pragma comment(lib, "Ws2_32.lib") // лінкуємо бібліотеку Windows Sockets

using namespace std;

void main()
{
    const int MAXSTRLEN = 255;
    WSADATA wsaData;// структура для зберігання інформації
    // про ініціалізацію сокетів
    SOCKET _socket; // дескриптор слухаючого сокету
    SOCKET acceptSocket;// дескриптор сокету, який пов'язаний із клієнтом 
    sockaddr_in addr;   // локальна адреса і порт
    // ініціалізація сокетів
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    // створюємо потоковий сокет, протокол TCP
    _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    // Сімейство адрес IPv4
    addr.sin_family = AF_INET;
    /* Перетворимо адресу на правильну
       структуру зберігання адрес, результат зберігаємо у полі sin_addr */
    inet_pton(AF_INET, "0.0.0.0", &addr.sin_addr);
    // Вказуємо порт. 
    // Функція htons виконує перетворення числа в
    // мережевий порядок байтів, оскільки стандартним мережевим
    // є порядок від старшого до молодшого
    addr.sin_port = htons(20000);
    // біндимо сокет до вказаної адреси та порту
    bind(_socket, (SOCKADDR*)&addr, sizeof(addr));
    // відкриваємо сокет у режимі прослуховування вхідних підключень
    listen(_socket, 1);
    cout << "Server is started\n";
    // очікуємо на підключення клієнта.
    // у разі успішного підключення - отримуємо новий сокет
    // через який відбуватиметься обмін повідомленнями
    // з клієнтом.
    // Функція є блокуючою.
    acceptSocket = accept(_socket, NULL, NULL);

    char buf[MAXSTRLEN];
    // Отримуємо повідомлення від клієнта
    // Функція є блокуючою
    int i = recv(acceptSocket, buf, MAXSTRLEN, 0);
    buf[i] = '\0';
    cout << buf << endl;
    const char* text = "Hello from server!";
    // Надсилаємо повідомлення клієнту
    send(acceptSocket, text, strlen(text), 0);

    // Закриваємо сокети
    closesocket(acceptSocket);
    closesocket(_socket);
    // Звільняємо ресурси
    WSACleanup();
    system("pause");
}