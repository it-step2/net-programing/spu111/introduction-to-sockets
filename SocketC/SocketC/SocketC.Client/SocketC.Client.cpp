﻿#include <iostream>
#include <string>
#include "WinSock2.h" // тут знаходяться оголошення, необхідні
// для Winsock 2 API.
#include <ws2tcpip.h> // містить функції для роботи з адресами
                      // напр. inet_pton
#pragma comment(lib, "Ws2_32.lib") // лінкуємо бібліотеку Windows Sockets

using namespace std;

int main()
{
    const int MAXSTRLEN = 255;
    WSADATA wsaData;  // структура для зберігання інформації
    // про ініціалізацію сокетів
    SOCKET _socket;   // дескриптор сокету
    sockaddr_in addr; // локальна адреса і порт
    // ініціалізація сокетів
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    // створюємо потоковий сокет, протокол TCP
    _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    // Сімейство адрес IPv4
    addr.sin_family = AF_INET;
    /* Перетворимо адресу на правильну
       структуру зберігання адрес, результат зберігаємо у полі sin_addr */
    inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);
    // Вказуємо порт. 
    // Функція htons виконує перетворення числа в
    // мережевий порядок байт, оскільки стандартним мережевим 
    // є порядок від старшого до молодшого
    addr.sin_port = htons(20000);
    // виконуємо підключення до сервера, вказуючи
    // дескриптор сокету, адресу та розмір структури
    connect(_socket, (SOCKADDR*)&addr, sizeof(addr));

    char buf[MAXSTRLEN];
    const char* text = "Hello world!";

    cout << "\nPress Enter to send 'Hello world!' to server\n";
    // Очікуємо підтвердження надсилання повідомлення користувачем
    // (натискання клавіші)
    cin.get();

    // Надсилаємо повідомлення на вказаний сокет
    send(_socket, text, strlen(text), 0);

    // Приймаємо повідомлення від сервера
    // Функція є блокуючою
    int i = recv(_socket, buf, MAXSTRLEN, 0);
    buf[i] = '\0';

    cout << buf << endl;

    // Закриваємо сокет
    closesocket(_socket);
    // Звільнюємо ресурси
    WSACleanup();
    system("pause");
}