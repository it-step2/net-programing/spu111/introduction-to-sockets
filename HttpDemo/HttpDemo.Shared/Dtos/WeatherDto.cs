﻿namespace HttpDemo.Shared.Dtos;

public sealed record WeatherDto(int Id, string City, double Temperature);
