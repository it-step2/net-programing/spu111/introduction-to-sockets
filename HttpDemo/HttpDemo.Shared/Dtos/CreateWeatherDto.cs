﻿namespace HttpDemo.Shared.Dtos;

public class CreateWeatherDto
{
    public string City { get; set; } = null!;
    public double Temperature { get; set; }
}