﻿using System.Net.Http.Json;
using HttpDemo.Shared.Dtos;

namespace HttpDemo.Client;

public sealed class Client
{
    private readonly HttpClient httpClient;
    
    public Client(string baseUrl)
    {
        httpClient = new HttpClient
        {
            BaseAddress = new Uri(baseUrl)
        };
    }

    public async Task<string> CreateAsync(CreateWeatherDto createWeatherDto)
    {
        var content = JsonContent.Create(createWeatherDto);
        
        var response = await httpClient.PostAsync("/api/weather", content);
        if (!response.IsSuccessStatusCode)
            return $"Your request failed with status code {response.StatusCode}";
        
        var weatherDto = await response.Content.ReadFromJsonAsync<WeatherDto>();
        return $"Weather with id {weatherDto?.Id} was created";
    }
    
    public async Task<string> UpdateAsync(int id, WeatherDto updateWeatherDto)
    {
        var content = JsonContent.Create(updateWeatherDto);
        
        var response = await httpClient.PutAsync($"/api/weather/{id}", content);
        return response.IsSuccessStatusCode
            ? $"Weather with id {id} was updated"
            : $"Your request failed with status code {response.StatusCode}";
    }
    
    public async Task<string> DeleteAsync(int id)
    {
        var response = await httpClient.DeleteAsync($"/api/weather/{id}");
        return response.IsSuccessStatusCode
            ? $"Weather with id {id} was deleted"
            : $"Your request failed with status code {response.StatusCode}";
    }
    
    public async Task<string> GetAsync(int id)
    {
        var response = await httpClient.GetAsync($"/api/weather/{id}");
        if (!response.IsSuccessStatusCode)
            return $"Your request failed with status code {response.StatusCode}";
        
        var weatherDto = await response.Content.ReadFromJsonAsync<WeatherDto>();
        return $"Weather with id {weatherDto?.Id} (City: {weatherDto?.City}, Temperature: {weatherDto?.Temperature}) was found";
    }
    
    public async Task<string> GetAllAsync()
    {
        var response = await httpClient.GetAsync("/api/weather");
        if (!response.IsSuccessStatusCode)
            return $"Your request failed with status code {response.StatusCode}";
        
        var weatherDtos = await response.Content.ReadFromJsonAsync<List<WeatherDto>>();
        var result = weatherDtos?.Aggregate("Weather list:", (current, weatherDto) => 
            current + $"\nWeather with id {weatherDto.Id} (City: {weatherDto.City}, Temperature: {weatherDto.Temperature})");
        return result ?? "Weather list is empty";
    }
}