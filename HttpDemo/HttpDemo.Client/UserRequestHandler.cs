﻿using HttpDemo.Shared.Dtos;

namespace HttpDemo.Client;

public sealed class UserRequestHandler
{
    private readonly Client client;
    
    public UserRequestHandler(Client client)
    {
        this.client = client;
    }
    
    public async Task<bool> HandleAsync(UserRequest request)
    {
        return request switch
        {
            UserRequest.Create => await CreateAsync(),
            UserRequest.Update => await UpdateAsync(),
            UserRequest.Delete => await DeleteAsync(),
            UserRequest.Get => await GetAsync(),
            UserRequest.GetAll => await GetAllAsync(),
            UserRequest.Exit => false,
            _ => throw new ArgumentOutOfRangeException(nameof(request), request, null)
        };
    }
    
    private async Task<bool> CreateAsync()
    {
        Console.WriteLine("Enter city:");
        var city = Console.ReadLine();
        if (string.IsNullOrWhiteSpace(city))
        {
            Console.WriteLine("Invalid city");
            return false;
        }
        
        Console.WriteLine("Enter temperature:");
        var temperature = Console.ReadLine();
        if (!double.TryParse(temperature, out var parsedTemperature))
        {
            Console.WriteLine("Invalid temperature");
            return false;
        }
        
        var message = await client.CreateAsync(new CreateWeatherDto
        {
            City = city,
            Temperature = parsedTemperature
        });
        Console.WriteLine(message);
        return true;
    }
    
    private async Task<bool> UpdateAsync()
    {
        Console.WriteLine("Enter id:");
        var id = Console.ReadLine();
        if (!int.TryParse(id, out var parsedId))
        {
            Console.WriteLine("Invalid id");
            return false;
        }
        
        Console.WriteLine("Enter city:");
        var city = Console.ReadLine();
        if (string.IsNullOrWhiteSpace(city))
        {
            Console.WriteLine("Invalid city");
            return false;
        }
        
        Console.WriteLine("Enter temperature:");
        var temperature = Console.ReadLine();
        if (!double.TryParse(temperature, out var parsedTemperature))
        {
            Console.WriteLine("Invalid temperature");
            return false;
        }
        
        var message = await client.UpdateAsync(parsedId, new WeatherDto(parsedId, city, parsedTemperature));
        Console.WriteLine(message);
        return true;
    }
    
    private async Task<bool> DeleteAsync()
    {
        Console.WriteLine("Enter id:");
        var id = Console.ReadLine();
        if (!int.TryParse(id, out var parsedId))
        {
            Console.WriteLine("Invalid id");
            return false;
        }
        
        var message = await client.DeleteAsync(parsedId);
        Console.WriteLine(message);
        return true;
    }
    
    private async Task<bool> GetAsync()
    {
        Console.WriteLine("Enter id:");
        var id = Console.ReadLine();
        if (!int.TryParse(id, out var parsedId))
        {
            Console.WriteLine("Invalid id");
            return false;
        }
        
        var message = await client.GetAsync(parsedId);
        Console.WriteLine(message);
        return true;
    }
    
    private async Task<bool> GetAllAsync()
    {
        var message = await client.GetAllAsync();
        Console.WriteLine(message);
        return true;
    }
}