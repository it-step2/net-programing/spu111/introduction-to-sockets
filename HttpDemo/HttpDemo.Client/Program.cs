﻿using HttpDemo.Client;

var client = new Client("https://localhost:7083/");
var userRequestHandler = new UserRequestHandler(client);

Console.WriteLine("Instructions:");
Console.WriteLine("1. Create a new weather: create <city> <temperature>");
Console.WriteLine("2. Update an existing weather: update <id> <city> <temperature>");
Console.WriteLine("3. Delete an existing weather: delete <id>");
Console.WriteLine("4. Get an existing weather: get <id>");
Console.WriteLine("5. Get all existing weathers: getall");
Console.WriteLine("6. Exit: exit");
Console.WriteLine();

while (true)
{
    Console.WriteLine("Enter what you need...");
    var input = Console.ReadLine();

    if (!int.TryParse(input, out var instruction) || !Enum.IsDefined(typeof(UserRequest), instruction))
    {
        Console.WriteLine("Invalid input");
        continue;
    }
    
    var result = await userRequestHandler.HandleAsync((UserRequest)instruction);
    if (!result)
        break;
}

