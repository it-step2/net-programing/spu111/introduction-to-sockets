﻿namespace HttpDemo.Client;

public enum UserRequest
{
    Create = 1,
    Update,
    Delete,
    Get,
    GetAll,
    Exit
}