﻿using HttpDemo.Shared.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace HttpDemo.Server.Controllers;

public sealed class WeatherController : Controller
{
    private static readonly IDictionary<int, WeatherDto> WeatherData = new Dictionary<int, WeatherDto>
    {
        { 1, new WeatherDto(1, "Kyiv", 15) },
        { 2, new WeatherDto(2, "Kharkiv", 17) },
        { 3, new WeatherDto(3, "London", 20) },
        { 4, new WeatherDto(4, "Sidney", 27) },
        { 5, new WeatherDto(5, "New York", 13) },
        { 6, new WeatherDto(6, "Lviv", 10) },
    };

    private static int lastId = 6;
    
    public WeatherController(ILogger<WeatherController> logger) : base(logger)
    {
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        return Ok(WeatherData.Values);
    }
    
    [HttpGet("{id:int}")]
    public IActionResult GetById([FromRoute] int id)
    {
        if (!WeatherData.TryGetValue(id, out var weather))
            return NotFound();

        return Ok(weather);
    }
    
    [HttpPost]
    public IActionResult Create([FromBody] CreateWeatherDto weather)
    {
        lastId++;
        var newWeather = new WeatherDto(lastId, weather.City, weather.Temperature);
        WeatherData.Add(newWeather.Id, newWeather);
        return Created($"/api/weather/{lastId}", newWeather);
    }
    
    [HttpPut("{id:int}")]
    public IActionResult Update([FromRoute] int id, [FromBody] WeatherDto weather)
    {
        if (!WeatherData.ContainsKey(id))
            return NotFound();

        WeatherData[id] = weather;
        return Ok();
    }
    
    [HttpDelete("{id:int}")]
    public IActionResult Delete([FromRoute] int id)
    {
        if (!WeatherData.ContainsKey(id))
            return NotFound();

        WeatherData.Remove(id);
        return Ok();
    }
}