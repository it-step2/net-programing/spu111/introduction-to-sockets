﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UdpBroadcastDemo;

public class CustomUdpClient : IDisposable
{
    private readonly Encoding encoding;
    
    public IPEndPoint LocalEndPoint { get; }
    public UdpClient Client { get; }
    
    public CustomUdpClient(int port, Encoding? encoding = null)
    {
        this.encoding = encoding ?? Encoding.ASCII;
        Client = new UdpClient(port, AddressFamily.InterNetworkV6);
        LocalEndPoint = Client.Client.LocalEndPoint as IPEndPoint ?? throw new InvalidOperationException();
    }

    public void Send(string message, IPEndPoint remoteEndPoint)
    {
        var bytes = encoding.GetBytes(message);
        Client.Send(bytes, bytes.Length, remoteEndPoint);
    }
    
    public string Receive(out IPEndPoint remoteEndPoint, int port)
    {
        remoteEndPoint = new IPEndPoint(IPAddress.IPv6Any, port);
        var bytes = Client.Receive(ref remoteEndPoint);
        return encoding.GetString(bytes);
    }

    public void Dispose()
    {
        Client.Dispose();
    }
}