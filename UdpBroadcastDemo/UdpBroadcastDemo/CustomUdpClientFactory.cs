﻿namespace UdpBroadcastDemo;

public static class CustomUdpClientFactory
{
    public static CustomUdpClient Create(int id)
    {
        return new CustomUdpClient(5000 + id);
    }
}