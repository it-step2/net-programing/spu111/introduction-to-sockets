﻿using SimpleChatDemo.Server;

var server = new Server("127.0.0.1", 5000, 5001, 5999);
server.Start();
Console.WriteLine("Enter something to stop the server...");
Console.ReadKey();
await server.StopAsync();