﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using SimpleChatDemo.Shared.Extensions;

namespace SimpleChatDemo.Server;

public sealed class Server
{
    private readonly IPAddress ipAddress;
    private readonly int port;
    private Socket? socket;
    private bool isStarted;

    private readonly Encoding encoding;

    private Task? startTask;
    private readonly CancellationTokenSource cancellationTokenSource;
    private CancellationToken? cancellationToken;
    
    private readonly IDictionary<string, ClientServer?> clientPorts;
    private readonly IDictionary<string, Task?> clientTasks;

    public Server(string ip, int port, int startPort, int endPort, Encoding? encoding = null)
    {
        ipAddress = IPAddress.Parse(ip);
        this.port = port;
        var addresses = Enumerable.Range(startPort, endPort - startPort + 1)
            .Select((p, index) => $"127.0.0.{index + 2}:{p}")
            .ToList();
        
        clientPorts = addresses.ToDictionary(address => address, _ => (ClientServer?)null);
        clientTasks = addresses.ToDictionary(address => address, _ => (Task?)null);
        
        isStarted = false;
        this.encoding = encoding ?? Encoding.UTF8;
        cancellationTokenSource = new CancellationTokenSource();
    }

    public void Start()
    {
        if (socket is not null)
            throw new InvalidOperationException("Server is already started");

        socket = CreateServer(ipAddress, port);
        
        cancellationToken = cancellationTokenSource.Token;
        startTask = StartServerAsync(socket, $"{ipAddress}:{port}", true, cancellationToken.Value);
    }

    private async Task StartServerAsync(Socket server, string address, bool isMain, CancellationToken token)
    {
        Console.WriteLine("Server is waiting for connections on: " + address);
        if (isMain)
            isStarted = true;
        
        var (ip, port) = GetAddressParts(address);

        while (isStarted)
        {
            try
            {
                var client = await server.AcceptAsync(token);
                Console.WriteLine(port + ": start communication with client " + client.RemoteEndPoint);

                if (!isMain)
                {
                    var cs = clientPorts[address]!;
                    cs.SetData(ip, port, client, server);
                    await CommunicateAsync(address, client, token);
                    clientPorts[address]!.Dispose();
                    break;
                }

                var clientServer = await GiveAddressAndStartCommunicateAsync(client, token);
                if (clientServer.Socket is null)
                {
                    Console.WriteLine("No free ports for client " + client.RemoteEndPoint);
                    continue;
                }

                clientTasks[clientServer.Address] =
                    StartServerAsync(clientServer.Socket, clientServer.Address, false, token);
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Server is stopped");
                break;
            }
            catch (SocketException ex)
            {
                Console.WriteLine($"SocketException for {address}: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        if (!isMain)
            clientTasks[address] = null;
        else
            Console.WriteLine($"{address} left the the loop");
    }

    public async Task StopAsync()
    {
        if (socket is null || startTask is null)
            return;
        
        cancellationTokenSource.Cancel();

        socket.Close();
        socket = null;
        isStarted = false;
        await startTask;
    }
    
    private async Task<(Socket? Socket, string Address)> GiveAddressAndStartCommunicateAsync(Socket client, CancellationToken token)
    {
        if (clientPorts.All(p => p.Value is not null && p.Value.State != ClientServerState.Disconnected))
        {
            await client.SendMessageAsync("0", encoding, token);
            return (null, string.Empty);
        }
        var address = clientPorts.First(p => p.Value is null || p.Value.State == ClientServerState.Disconnected).Key;
        Console.WriteLine(address);
        var (ipAddress, port) = GetAddressParts(address);
        var server = CreateServer(ipAddress, port);
        await client.SendMessageAsync(address, encoding, token);
        clientPorts[address] = new ClientServer();
        return (server, address);
    }
    
    private async Task CommunicateAsync(string serverIpAddress, Socket client, CancellationToken token)
    {
        var message = string.Empty;
        while (ContinueCommunication(message))
        {
            message = await client.ReceiveMessageAsync(encoding, token);
            Console.WriteLine($"Received by {serverIpAddress} from {client.RemoteEndPoint}: {message}");

            await client.SendMessageAsync("Hello from server", encoding, token);
        }
        
        Console.WriteLine("Stop communication with client " + client.RemoteEndPoint);
        
    }

    private static bool ContinueCommunication(string message) =>
        !message.Equals("exit", StringComparison.OrdinalIgnoreCase);

    private static Socket CreateServer(IPAddress ipAddress, int port, int backlog = 10)
    {
        var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        var ipEndPoint = new IPEndPoint(ipAddress, port);
        socket.Bind(ipEndPoint);
        socket.Listen(backlog);
        return socket;
    }
    
    private static (IPAddress, int) GetAddressParts(string address)
    {
        var addressParts = address.Split(':');
        var port = addressParts.Last();
        var ip = addressParts.First();
        return (IPAddress.Parse(ip), int.Parse(port));
    }
}