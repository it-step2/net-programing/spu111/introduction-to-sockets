﻿using System.Net;
using System.Net.Sockets;

namespace SimpleChatDemo.Server;

public class ClientServer : IDisposable
{
    public ClientServerState State { get; private set; } = ClientServerState.WaitingForConnection;
    
    public IPAddress IpAddress { get; private set; } = null!;
    public int Port { get; private set; }
    public Socket ClientSocket { get; private set; } = null!;
    public Socket ServerSocket { get; private set; } = null!;
    
    public void SetData(IPAddress ipAddress, int port, Socket client, Socket server)
    {
        IpAddress = ipAddress;
        Port = port;
        ClientSocket = client;
        ServerSocket = server;
        State = ClientServerState.Connected;
    }

    public void Dispose()
    {
        State = ClientServerState.Disconnected;
        
        ClientSocket.Dispose();
        ServerSocket.Dispose();
        
        ClientSocket = null!;
        ServerSocket = null!;
        IpAddress = null!;
    }
}

public enum ClientServerState
{
    WaitingForConnection,
    Connected,
    Disconnected
}
