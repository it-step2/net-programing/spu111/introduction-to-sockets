﻿using System.Net.Sockets;
using System.Text;

namespace SimpleChatDemo.Shared.Extensions;

public static class SocketExtensions
{
    public static async Task<string> ReceiveMessageAsync(this Socket socket, Encoding? encoding = null, CancellationToken cancellationToken = default)
    {
        encoding ??= Encoding.UTF8;
        var buffer = new byte[1024];
        var receivedBytesNumber = await socket.ReceiveAsync(buffer, cancellationToken: cancellationToken);
        return encoding.GetString(buffer, 0, receivedBytesNumber);
    }
    
    public static async Task SendMessageAsync(this Socket socket, string message, Encoding? encoding = null, CancellationToken cancellationToken = default)
    {
        encoding ??= Encoding.UTF8;
        var buffer = encoding.GetBytes(message);
        await socket.SendAsync(buffer, cancellationToken: cancellationToken);
    }
}