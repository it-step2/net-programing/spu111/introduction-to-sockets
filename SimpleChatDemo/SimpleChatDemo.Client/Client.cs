﻿using System.Net;
using System.Net.Sockets;
using SimpleChatDemo.Shared.Extensions;

namespace SimpleChatDemo.Client;

public class Client
{
    private const string DefaultIpAddress = "127.0.0.1";
    private const int DefaultPort = 5000;

    private readonly IPAddress ipAddress;
    private readonly EndPoint endPoint;
    private Socket? client;

    public int Id { get; }
    
    public bool Connected => client?.Connected ?? false;

    public Client(int id, string ipAddressString = DefaultIpAddress, int port = DefaultPort)
    {
        Id = id;

        ipAddress = IPAddress.Parse(ipAddressString);
        endPoint = new IPEndPoint(ipAddress, port);
    }

    public async Task ConnectAsync()
    {
        var isSuccessful = false;
        while (!isSuccessful)
        {
            try
            {
                client = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                await client.ConnectAsync(endPoint);

                var message = await client.ReceiveMessageAsync();
                var addressParts = message.Split(':');
                var ip = addressParts.First();
                var portString = addressParts.Last();
                if (!int.TryParse(portString, out var port) || port <= 0 || !IPAddress.TryParse(ip, out var ipAddress))
                {
                    Console.WriteLine($"Client {Id}: All ports are busy. Try next time");
                    break;
                }
                client.Shutdown(SocketShutdown.Both);
                client.Close();
                
                client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                await client.ConnectAsync(new IPEndPoint(ipAddress, port));
                isSuccessful = true;
            }
            catch (SocketException)
            {
                Console.WriteLine($"Client {Id}: Connection failed. Retrying...");
                Thread.Sleep(1000);
            }
        }
    }

    public async Task SendAsync(string message)
    {
        await client!.SendMessageAsync(message);
        Console.WriteLine("Sent: " + message);
    }

    public async Task<string> ReceiveAsync()
    {
        var receivedMessage = await client!.ReceiveMessageAsync();
        Console.WriteLine("Received: " + receivedMessage);
        return receivedMessage;
    }

    public async Task<string> SendAndReceiveAsync(string message)
    {
        await SendAsync(message);
        return await ReceiveAsync();
    }

    public void Disconnect()
    {
        client!.Shutdown(SocketShutdown.Both);
        client.Close();
        client = null;
    }

    public void Dispose()
    {
        Disconnect();
        client?.Dispose();
    }

}