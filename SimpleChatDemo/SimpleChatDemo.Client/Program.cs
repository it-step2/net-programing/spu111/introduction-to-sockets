﻿using SimpleChatDemo.Client;

var clients = Enumerable.Range(1, 10).Select(id => new Client(id));
await Parallel.ForEachAsync(clients, async (client, _) =>
{
    var random = new Random();
    await client.ConnectAsync();

    var sleepTime = random.Next(1000, 5000);
    Console.WriteLine($"Client {client.Id} will sleep for {sleepTime} ms");
    Thread.Sleep(sleepTime);

    if (!client.Connected)
        Thread.Sleep(1000);
    
    var messageCount = random.Next(1, 10);
    for (var i = 0; i < messageCount; i++)
        await client.SendAndReceiveAsync("Hello from client " + client.Id);
    await client.SendAsync("exit");

    client.Disconnect();
});

Console.ReadKey();