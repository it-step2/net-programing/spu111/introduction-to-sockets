﻿using System.Net;
using System.Net.Sockets;
using TcpListenerClientDemo.SocketExtensions;

var address = IPAddress.Parse("127.0.0.1");
var listener = new TcpListener(address, 5000);

listener.Start();
using var client = await listener.AcceptSocketAsync();

var message = await client.ReceiveMessageAsync();
Console.WriteLine($"Received: {message}");

await client.SendMessageAsync("Hello from the server!");

listener.Stop();
