﻿using System.Net;
using System.Net.Sockets;
using System.Text;

using var client = new TcpClient();

var address = IPAddress.Parse("127.0.0.1");
await client.ConnectAsync(address, 5000);

var stream = client.GetStream();

const string message = "Hello from the client!";
var sendBytes = Encoding.UTF8.GetBytes(message);
await stream.WriteAsync(sendBytes);

var receiveBytes = new byte[1024];
var received = await stream.ReadAsync(receiveBytes);
var receivedMessage = Encoding.UTF8.GetString(receiveBytes, 0, received);
Console.WriteLine($"Received: {receivedMessage}");
