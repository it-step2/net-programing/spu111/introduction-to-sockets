﻿using System.Net.Sockets;
using System.Text;

namespace TcpListenerClientDemo.SocketExtensions;

public static class SocketExtensions
{
    public static string ReceiveMessage(this Socket socket, Encoding? encoding = null)
    {
        encoding ??= Encoding.UTF8;
        
        var buffer = new byte[1024];
        var received = socket.Receive(buffer);
        return encoding.GetString(buffer, 0, received);
    }

    public static void SendMessage(this Socket socket, string message, Encoding? encoding = null)
    {
        encoding ??= Encoding.UTF8;

        var bytes = encoding.GetBytes(message);
        socket.Send(bytes);
    }
    
    public static async Task<string> ReceiveMessageAsync(this Socket socket, Encoding? encoding = null)
    {
        encoding ??= Encoding.UTF8;
        
        var buffer = new byte[1024];
        var arraySegment = new ArraySegment<byte>(buffer);
        var received = await socket.ReceiveAsync(arraySegment, SocketFlags.None);
        return encoding.GetString(buffer, 0, received);
    }

    public static async Task SendMessageAsync(this Socket socket, string message, Encoding? encoding = null)
    {
        encoding ??= Encoding.UTF8;

        var bytes = encoding.GetBytes(message);
        var arraySegment = new ArraySegment<byte>(bytes);
        await socket.SendAsync(arraySegment, SocketFlags.None);
    }
}