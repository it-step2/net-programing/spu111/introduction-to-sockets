﻿using System.Net;
using System.Net.Sockets;
using System.Text;

var host = Dns.GetHostEntry("localhost");
var ipAddress = host.AddressList[0];
var endPoint = new IPEndPoint(ipAddress, 5000);

using var client = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
await client.ConnectAsync(endPoint);

while (true)
{
	Console.Write("Enter a message: ");
	var message = Console.ReadLine();
	if (string.IsNullOrWhiteSpace(message))
	{
		Console.WriteLine("Invalid message");
		continue;
	}	

	var sendBytes = Encoding.ASCII.GetBytes(message);
	await client.SendAsync(sendBytes);
	Console.WriteLine("Sent: " + message);

	var buffer = new byte[1024];
	var receivedBytes = await client.ReceiveAsync(buffer);
	var receivedMessage = Encoding.ASCII.GetString(buffer, 0, receivedBytes);
	Console.WriteLine("Received: " + receivedMessage);

	if (receivedMessage.Equals("exit", StringComparison.OrdinalIgnoreCase))
	{
		Console.WriteLine("Exiting...");
		break;
	}
}

client.Shutdown(SocketShutdown.Both);
