﻿using System.Net;
using System.Net.Sockets;
using System.Text;

var host = Dns.GetHostEntry("localhost");
var ipAddress = host.AddressList[0];
var endPoint = new IPEndPoint(ipAddress, 5000);

using var socket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
socket.Bind(endPoint);
socket.Listen(10);

using var server = await socket.AcceptAsync();
while (true)
{
	var buffer = new byte[1024];
	var receivedBytes = await server.ReceiveAsync(buffer);
	var receivedMessage = Encoding.ASCII.GetString(buffer, 0, receivedBytes);
	Console.WriteLine("Received: " + receivedMessage);

	var sendBytes = Encoding.ASCII.GetBytes(receivedMessage);
	await server.SendAsync(sendBytes);
	Console.WriteLine("Sent: " + receivedMessage);

	if (receivedMessage.Equals("exit", StringComparison.OrdinalIgnoreCase))
	{
		Console.WriteLine("Exiting...");
		break;
	}
}
