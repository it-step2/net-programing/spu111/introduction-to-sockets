﻿/*
Розробіть набір консольних додатків. Перший додаток: 
серверний додаток, який на запити клієнта повертає поточний 
час або дату на сервері. Другий додаток: клієнтський додаток, що 
запитує дату або час. Користувач з клавіатури визначає, що 
потрібно запросити. Після відправлення дати або часу, сервер 
розриває з'єднання. Клієнтський додаток відображає отримані 
дані. 
Використовуйте механізм синхронних сокетів.
 */

using System.Net;
using System.Net.Sockets;
using System.Text;

var host = Dns.GetHostEntry("localhost");
var ipAddress = host.AddressList[0];
var endPoint = new IPEndPoint(ipAddress, 5000);

using var socket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
socket.Bind(endPoint);
socket.Listen(10);

using var server = socket.Accept();
while (true)
{
    var buffer = new byte[1024];
    var receivedBytes = server.Receive(buffer);
    var receivedMessage = Encoding.ASCII.GetString(buffer, 0, receivedBytes);
    Console.WriteLine("Received: " + receivedMessage);
    
    var now = DateTime.Now;
    var shouldExit = receivedMessage.Equals("exit", StringComparison.OrdinalIgnoreCase);
    if (shouldExit)
    {
        Console.WriteLine("Exiting...");
        var exitBytes = Encoding.ASCII.GetBytes("exit");
        server.Send(exitBytes);
        break;
    }
    
    var result = receivedMessage.ToLower() switch
    {
        "date" => now.ToString("dd/MM/yyyy"),
        "time" => now.ToString("hh:mmm"),
        _ => "Incorrect argument, please, provide 'date' or 'time' values"
    };
    
    var sendBytes = Encoding.ASCII.GetBytes(result);
    server.Send(sendBytes);
    Console.WriteLine("Sent: " + result);
}
