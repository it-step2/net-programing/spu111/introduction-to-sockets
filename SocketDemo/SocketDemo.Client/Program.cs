﻿/*
Розробіть набір консольних додатків. Перший додаток: 
серверний додаток, який на запити клієнта повертає поточний 
час або дату на сервері. Другий додаток: клієнтський додаток, що 
запитує дату або час. Користувач з клавіатури визначає, що 
потрібно запросити. Після відправлення дати або часу, сервер 
розриває з'єднання. Клієнтський додаток відображає отримані 
дані. 
Використовуйте механізм синхронних сокетів.
 */

using System.Net;
using System.Net.Sockets;
using System.Text;

var host = Dns.GetHostEntry("localhost");
var ipAddress = host.AddressList[0];
var endPoint = new IPEndPoint(ipAddress, 5000);

using var client = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
client.Connect(endPoint);

while (true)
{
    Console.WriteLine("Enter message...");
    var message = Console.ReadLine();

    var acceptedMessages = new[] { "date", "time", "exit" };
    if (string.IsNullOrWhiteSpace(message) || 
        !acceptedMessages.Contains(message, StringComparer.OrdinalIgnoreCase))
    {
        Console.WriteLine("Invalid argument. Please, try: date, time or exit");
        continue;
    }

    Console.WriteLine("Sent: " + message);
    var sendBytes = Encoding.ASCII.GetBytes(message);
    client.Send(sendBytes);
    
    var buffer = new byte[1024];
    var receivedBytes = client.Receive(buffer);
    var receivedMessage = Encoding.ASCII.GetString(buffer, 0, receivedBytes);
    Console.WriteLine("Received: " + receivedMessage);
    if (receivedMessage.Equals("exit", StringComparison.OrdinalIgnoreCase))
    {
        Console.WriteLine("Exiting...");
        break;
    }
}

client.Shutdown(SocketShutdown.Both);
