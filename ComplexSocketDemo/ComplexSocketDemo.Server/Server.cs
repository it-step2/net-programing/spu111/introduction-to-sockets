﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ComplexSocketDemo.Server;

public class Server : IDisposable
{
    private const string DefaultHost = "localhost";
    private const int DefaultPort = 5000;
    
    private readonly EndPoint endPoint;
    private Socket? socket;
    private Socket? server;

    public Server(string hostName = DefaultHost, int port = DefaultPort)
    {
        var host = Dns.GetHostEntry(hostName);
        var ipAddress = host.AddressList[0];
        endPoint = new IPEndPoint(ipAddress, port);
    }

    public void Start()
    {
        var starterSocket = CreateSocket();
        starterSocket.Bind(endPoint);
        starterSocket.Listen(10);
        server = starterSocket.Accept();
        Handle();
    }

    public void Finish()
    {
        server?.Close();
        server = null;
    }

    private Socket CreateSocket()
    {
        return socket ??= new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
    }

    private void Handle()
    {
        if (server is null)
            return;
        
        while (true)
        {
            try
            {
                var buffer = new byte[1024];
                var receivedBytes = server.Receive(buffer);
                var receivedMessage = Encoding.ASCII.GetString(buffer, 0, receivedBytes);
                Console.WriteLine(receivedMessage);

                var messageBack = DateTime.UtcNow.ToString("dd/MM/yyyy hh:mm") + " " + receivedMessage;
                var sendBytes = Encoding.ASCII.GetBytes(messageBack);
                server.Send(sendBytes);
                Console.WriteLine("Sent: " + messageBack);

                if (receivedMessage.Equals("exit", StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Exiting...");
                    break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }

    public void Dispose()
    {
        Finish();
        socket?.Dispose();
        server?.Dispose();
    }
}
