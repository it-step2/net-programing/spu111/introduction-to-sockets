﻿using System.Net;
using System.Net.Sockets;
using System.Text;

var endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11000);
using var client = new UdpClient(endPoint);

var message = Encoding.UTF8.GetBytes("Hello World");
var clientEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.2"), 11000);
client.Send(message, message.Length, clientEndPoint);

var remoteIpAddress = new IPEndPoint(IPAddress.Any, 11000);
var received = client.Receive(ref remoteIpAddress);
var receivedMessage = Encoding.UTF8.GetString(new ArraySegment<byte>(received, 0, received.Length));
Console.WriteLine("Received: " + receivedMessage);

Console.WriteLine("Closed");
