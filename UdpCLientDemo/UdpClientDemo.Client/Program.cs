﻿using System.Net;
using System.Net.Sockets;
using System.Text;

var clientEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.2"), 11000);
using var client = new UdpClient(clientEndPoint);

var message = Encoding.UTF8.GetBytes("Hello World from client");
client.Send(message, message.Length, "localhost", 11000);

var endPoint = new IPEndPoint(IPAddress.Any, 0);
var received = client.Receive(ref endPoint);
var receivedMessage = Encoding.UTF8.GetString(new ArraySegment<byte>(received, 0, received.Length));
Console.WriteLine("Received: " + receivedMessage);

message = Encoding.UTF8.GetBytes("Hello World 2 from client");
client.Send(message, message.Length, "localhost", 11000);

Console.WriteLine("Closed");
